import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  selectedValue:string='';

  stories = [
    {value: 'arts', viewValue: 'Arts'},
    {value: 'automobiles', viewValue: 'Automobiles'},
    {value: 'books', viewValue: 'Books'},
    {value: 'business', viewValue: 'Business'},
    {value: 'fashion', viewValue: 'Fashion'},
    {value: 'food', viewValue: 'Food'},
    {value: 'health', viewValue: 'Health'},
    {value: 'home', viewValue: 'Home'},
    {value: 'insider', viewValue: 'Insider'},
    {value: 'magazine', viewValue: 'Magazine'},
    {value: 'movies', viewValue: 'Movies'},
    {value: 'nyregion', viewValue: 'NY-Region'},
    {value: 'obituaries', viewValue: 'Obituaries'},
    {value: 'opinion', viewValue: 'Opinion'},
    {value: 'politics', viewValue: 'Politics'},
    {value: 'realestate', viewValue: 'Realestate'},
    {value: 'science', viewValue: 'Science'},
    {value: 'sports', viewValue: 'Sports'},
    {value: 'science', viewValue: 'Science'},
    {value: 'sundayreview', viewValue: 'SundayReview'},
    {value: 'technology', viewValue: 'Technology'},
    {value: 'sundayreview', viewValue: 'SundayReview'},
    {value: 'theater', viewValue: 'Theater'},
    {value: 't-magazine', viewValue: 'T-magazine'},
    {value: 'travel', viewValue: 'Travel'},
    {value: 'upshot', viewValue: 'Upshot'},
    {value: 'us', viewValue: 'US'},
    {value: 'world', viewValue: 'World'},

  ];
  section: any=[];
  image: any;
  grid = true;

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }
  getsection(){
    console.log(this.selectedValue);
    const params = new HttpParams().set(
      'api-key',
      'TwLq83Tj9OVB5rxqNTGGq7U1eWACuRfO'
    );
    const url = 'https://api.nytimes.com/svc/topstories/v2/'+this.selectedValue+'.json';

    this.http
      .get<any>(url, {params})
      .subscribe((response) => {
        this.section=response.results;
        this.image=this.section.multimedia[0]
        console.log(this.section);
      });
  }
  
}
