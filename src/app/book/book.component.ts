import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';

export interface UserData {
  display_name:string;
  newest_published_date: string;
  oldest_published_date: string;
  updated: string;
}

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit {
  displayedColumns = ['display_name', 'newest_published_date', 'oldest_published_date', 'updated'];
  dataSource:MatTableDataSource<UserData>;
  private _data: UserData[];
  resultsLength:number=0;
  @ViewChild(MatPaginator) paginator: MatPaginator ;
  loading: boolean;

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.loading=true;
    const params = new HttpParams().set(
      'api-key',
      'TwLq83Tj9OVB5rxqNTGGq7U1eWACuRfO'
    );

    const url = 'https://api.nytimes.com/svc/books/v3/lists/names.json';

    this.http
      .get<any>(url, {
        params,
      }).subscribe((data) => {
        this._data = data.results;
        this.resultsLength=data.results.length;
        this.dataSource = new MatTableDataSource(this._data);
        this.dataSource.paginator = this.paginator;
        this.loading=false;

      });
  }
  applyFilter($event:any) {
console.log($event);
    let filterValue=$event.target.value;
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  
  
  }


